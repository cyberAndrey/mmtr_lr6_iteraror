package com.company;

public class Main {

    private static Class<Integer> integer;

    public static void main(String[] args) {
        MyMass myMass = new MyMass(3, 3);
        printMass(myMass.mass);
        System.out.println("Прохожусь по строкам и, если элемент нечетный, приравниваю его к 0");
        MyIterator iterator = myMass.iterator();
        while (iterator.hasNext()) {
            int cur = iterator.next();
            if (cur % 2 != 0) iterator.remove();
            System.out.print(cur + " ");
        }
        System.out.println();
        printMass(myMass.mass);
        System.out.println("Попытка вызвать next после hasNext = false: ");
        iterator.next();
    }

    private static void printMass(int[][] mass) {
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++)
                System.out.print(mass[i][j] + " ");
            System.out.println(); }
    }

}



