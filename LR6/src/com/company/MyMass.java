package com.company;

import java.util.NoSuchElementException;
import java.util.Random;

public class MyMass {
    public int[][] mass;
    int massSize;

    public MyMass(int rows, int columns) {
        this.mass = new int[rows][columns];
        this.massSize = rows * columns;
        fillMass();
    }

    public MyIterator iterator() {
        return new MyItr();
    }

    private void fillMass() {
        Random random = new Random();
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                mass[i][j] = random.nextInt(100);
            }
        }
    }


    private class MyItr implements MyIterator {
        int cursor = 0;
        int currentRow = 0;
        int count = 0;

        MyItr() { }

        @Override
        public boolean hasNext() {
            return count != massSize;
        }

        @Override
        public int next() {
            count++;
            if (cursor != mass.length) return mass[currentRow][cursor++];
            else {
                cursor = 0;
                currentRow++;
                if (currentRow == mass[0].length) throw new NoSuchElementException();
                return mass[currentRow][cursor++];
            }
        }

        @Override
        public void remove() {
            mass[currentRow][cursor-1] = 0;
        }
    }
}
