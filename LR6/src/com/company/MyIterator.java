package com.company;

public interface MyIterator {

    boolean hasNext();

    int next();

    void remove();
}
